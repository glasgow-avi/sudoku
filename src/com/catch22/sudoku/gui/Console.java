package com.catch22.sudoku.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Console
{
	private int height, width;
	private String title;
	private JPanel grid, solveButton;
	protected JFrame window;
			
	public Console()
	{
		window = new JFrame();
		grid = new JPanel();
		grid.setLayout(new GridLayout(11, 11));
		window.setLayout(new FlowLayout());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setTitle(this.title);
		window.setVisible(true);		
		window.setSize(this.width, this.height);
		window.add(grid);
	}

	public Console(String title, Dimension d)
	{
		this.title = title;
		this.width = d.width;
		this.height = d.height;
		window = new JFrame();
		grid = new JPanel();
		solveButton = new JPanel();
		solveButton.setSize(new Dimension(50, 50));
		grid.setLayout(new GridLayout(12, 12));
		window.setLayout(new FlowLayout());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setTitle(this.title);
		window.setVisible(true);		
		window.setSize(this.width, this.height);
		window.add(grid);
	}
	
	public Console(String title, Point p)
	{
		this.title = title;
		window = new JFrame();
		grid = new JPanel();
		grid.setLayout(new GridLayout(11, 11));
		window.setLayout(new GridLayout(2, 1));
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setTitle(this.title);
		window.setVisible(true);
		window.setLocation(p.x, p.y);
		window.pack();
	}
		
	public void setSize(int width, int height)
	{
		this.width = width;
		this.height = height;
		window.setSize(width, height);
	}
	
	public void setSize()
	{
		window.pack();
	}
	
	public void setTitle(String title)
	{
		this.title = title;
		window.setTitle(this.title);
	}
	
	public void add(Component x)
	{
		grid.add(x);
	}
	
	public void addButton(Component s)
	{
		solveButton = new JPanel();
		solveButton.add(s);
		window.add(solveButton);
		solveButton.setSize(1, 1);
	}
	
	public void close()
	{
		window.dispose();
	}
	
	public Console(Console a)
	{
		this.height = a.height;
		this.width = a.width;
		this.title = a.title;
		this.grid = a.grid;
		this.window = a.window;
	}
}