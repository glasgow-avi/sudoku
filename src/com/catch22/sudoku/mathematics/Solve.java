package com.catch22.sudoku.mathematics;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.catch22.sudoku.data.Grid;
import com.catch22.sudoku.gui.Console;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JFrame;

public class Solve extends Grid
{	
	private static JTextField[][] fields = new JTextField[9][9];
	private static Console console;
	private static JButton solveButton;
	
	/*
	 * logic function to solve the given puzzle
	 * brute force method is used
	 */
	private void solve()
	{
		boolean didBacktrack = false; //direction of movement
		initializeKeys();

		for(int rowIndex = 0; rowIndex < 9; rowIndex++)
		{
			for(int colIndex = 0; colIndex < 9; colIndex++)
			{
				
				if(isKey(rowIndex, colIndex))
				{
					//moving backward
					if(didBacktrack)
					{
						if(colIndex == 0) //first column, decrement the row
						{
							rowIndex --;
							colIndex = 7;
						}
						
						else //simple decrement
							colIndex -= 2;
					}
						
					continue; //next iteration
				}
				
				//normal case
				if(!didBacktrack)
				{
					for(int c = 1; c <= 9; c++) //find the smallest value that's valid in this square
					{
						grid[rowIndex][colIndex] = c;
						if(valid(rowIndex, colIndex))
							break;
					}
					
					if(!valid(rowIndex, colIndex)) //no value is valid in this square
					{
						grid[rowIndex][colIndex] = 0;
						didBacktrack = true; //have to backtrack here, reverse the direction
						
						if(colIndex == 0) //first column, decrement the row
						{
							rowIndex --;
							colIndex = 7;
						}
						
						else //simple decrement
							colIndex -= 2;
					}
					
					continue; //next iteration
				}
				
				//below is the code for moving backward
								
				//find the next smallest value that's valid in this square
				while(++grid[rowIndex][colIndex] <= 9)
					if(valid(rowIndex, colIndex))
						break;
				
				if(!valid(rowIndex, colIndex)) //no value is valid in this square, have to further backtrack
				{
					grid[rowIndex][colIndex] = 0;
					
					if(colIndex == 0) //first column, decrement the row
					{
						rowIndex --;
						colIndex = 7;
					}
					
					else //simple decrement
						colIndex -= 2;
				}
				
				else //something is valid, move forward from here, again
					didBacktrack = false; //reverse the direction to forward
				
			} //close column loop
		} //close row loop
	} //close solve method
	
	public void display()
	{
		Console solution = new Console("Solution found", new Dimension(400, 400));
		
		for(int rowIndex = 0; rowIndex < 9; rowIndex ++)
		{
			for(int colIndex = 0; colIndex < 9; colIndex ++)
			{
				JLabel square = new JLabel(/*"  " + grid[rowIndex][colIndex]*/);
				square.setText(String.valueOf(grid[rowIndex][colIndex]));
				square.setPreferredSize(new Dimension(30, 30));
				solution.add(square);
				
				//Adding 3x3 box separators
				if((colIndex+1)%3 == 0 && colIndex != 0 && colIndex != 8)
				{
					JLabel separator = new JLabel();
					separator.setPreferredSize(new Dimension(30, 30));
					solution.add(separator);
				}
			}
			
			//Adding 3x3 box separators
			if((rowIndex+1)%3 == 0 && rowIndex != 0)
			{
				for(int i = 0; i < 11; i ++)
				{
					JLabel separator = new JLabel();
					separator.setPreferredSize(new Dimension(30, 30));
					solution.add(separator);
				}
			}
			solution.setSize();
		}

	}
	
	/*
	 * call super constructor
	 */
	public Solve(int[][] grid)
	{
		super(grid);
		solve();
	}
	
	public Solve(){super();}
	
	private void initializeKeys()
	{
		for(int rowIndex = 0; rowIndex < 9; rowIndex ++)
			for(int colIndex = 0; colIndex < 9; colIndex ++)
				keys[rowIndex][colIndex] = grid[rowIndex][colIndex] != 0 ? true : false;
				
	}
	
	public static void main(String[] args)
	{
		console = new Console("Sudoku Solver", new Dimension(400, 400));
			
		//Adding 81 text fields to the console
		for(int rowIndex = 0; rowIndex < 9; rowIndex ++)
		{
			for(int colIndex = 0; colIndex < 9; colIndex ++)
			{
				fields[rowIndex][colIndex] = new JTextField();
				fields[rowIndex][colIndex].setPreferredSize(new Dimension(30, 30));
				console.add(fields[rowIndex][colIndex]);
				
				//Adding 3x3 box separators
				if((colIndex+1)%3 == 0 && colIndex != 0 && colIndex != 8)
				{
					JLabel separator = new JLabel();
					separator.setPreferredSize(new Dimension(30, 30));
					console.add(separator);
				}
			}
			
			//Adding 3x3 box separators
			if((rowIndex+1)%3 == 0 && rowIndex != 0)
			{
				for(int i = 0; i < 11; i ++)
				{
					JLabel separator = new JLabel();
					separator.setPreferredSize(new Dimension(30, 30));
					console.add(separator);
				}
			}
		}
		
		//add a solve button
		solveButton = new JButton("Solve");
		solveButton.addActionListener(new Solve().new SolveEvent());
		console.addButton(solveButton);
		console.setSize();
	}

	public class SolveEvent implements ActionListener
	{		
		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			for(int rowIndex = 0; rowIndex < 9; rowIndex++)
			{
				for(int colIndex = 0; colIndex < 9; colIndex++)
				{
						try
						{
							grid[rowIndex][colIndex] = (int)fields[rowIndex][colIndex].getText().charAt(0) - 48;
						}
						catch(StringIndexOutOfBoundsException empty)
						{
							grid[rowIndex][colIndex] = 0;
						}
					}
				}
			
			if(!validGrid())
			{
				JFrame invalidFrame = new JFrame();
				String message = "Invalid Puzzle Entered";
				JLabel text = new JLabel("      " + message + "      ");
				invalidFrame.setTitle("Error");
				invalidFrame.setLayout(new FlowLayout(20));
				invalidFrame.setVisible(true);		
				invalidFrame.add(text);
				invalidFrame.pack();
				invalidFrame.setLocation(75, 200);
			}
			
			else
			{
				Solve solution = new Solve(grid);
				console.close();
				solution.display();
			}
		}
	}
}