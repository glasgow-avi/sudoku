package com.catch22.sudoku.data;

public class Grid
{
	public int[][] grid = new int[9][9];
	protected boolean[][] keys = new boolean[9][9];
	
	/* counts the number of repetitions of a number in a row or a column
	 * PARAMETERS:
	 * x: the row/column index to be searched
	 * val: the value to be counted
	 * rowCol: "row" to search a row, "col" to search a column
	 */
	protected int countRepetition(int x, int val, String rowCol)
	{
		int count = 0;
		for(int index = 0; index < 9; index++)
			if(grid[index][x] == val && rowCol == "col" || grid[x][index] == val && rowCol == "row")
				count++;
		
		return count - 1 ;			
	}

	/* counts the number of repetitions of a number in a 3x3 box
	 * PARAMETERS:
	 * startRowIndex: row index of the first square of the box to be searched
	 * startColIndex: column index of the first square of the box to be searched
	 * val: the value to be counted
	 */
	protected int countRepetition(int startRowIndex, int startColIndex, int val)
	{
		int count = 0;
		for(int rowIndex = startRowIndex; rowIndex < startRowIndex + 3; rowIndex++)
			for(int colIndex = startColIndex; colIndex < startColIndex + 3; colIndex++)
				if(grid[rowIndex][colIndex] == val)
					count++;
		
		return count - 1 ;		
	}
	
	protected boolean valid(int rowIndex, int colIndex)
	{
		if(grid[rowIndex][colIndex] == 0)
			return true;
		
		if(grid[rowIndex][colIndex] < 0 || grid[rowIndex][colIndex] > 9)
		{
			return false;
		}
		
		/*
		int count1 = countRepetition(rowIndex, grid[rowIndex][colIndex], "row");
		int count2 = countRepetition(colIndex, grid[rowIndex][colIndex], "col");
		int count3 = countRepetition(rowIndex / 3, colIndex / 3, grid[rowIndex][colIndex]);
	
		System.out.println(count1 + " " + count2 + " " + count3);
		*/
		
		int count = countRepetition(rowIndex, grid[rowIndex][colIndex], "row")
				+ countRepetition(colIndex, grid[rowIndex][colIndex], "col")
				+ countRepetition(rowIndex / 3 * 3, colIndex / 3 * 3, grid[rowIndex][colIndex]);
		
		return count == 0 ? true : false;
	}
	
	protected boolean isKey(int rowIndex, int colIndex)
	{
		return keys[rowIndex][colIndex];
	}
	
	protected boolean validGrid()
	{
		for(int rowIndex = 0; rowIndex < 9; rowIndex ++)
			for(int colIndex = 0; colIndex < 9; colIndex ++)
				if(!valid(rowIndex, colIndex))
					return false;					
		return true;
	}
	
	public Grid(int[][] grid) //constructor to initialize all squares to blank value
	{
		for(int rowIndex = 0; rowIndex < 9; rowIndex++)
		{
			for(int colIndex = 0; colIndex < 9; colIndex++)
			{
				this.grid[rowIndex][colIndex] = grid[rowIndex][colIndex];
				keys[rowIndex][colIndex] = grid[rowIndex][colIndex] != 0 ? true : false;
			}
		}
	}
	
	public Grid(){}
}